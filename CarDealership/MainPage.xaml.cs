﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using CarDealership.BusinessLogic;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace CarDealership
{

    public sealed partial class MainPage : Page
    {

        Dealership vehicles;
        public decimal DepPrice { get; set; }

        public MainPage()
        {
            this.InitializeComponent();
            vehicles = new Dealership();


        }
        /// <summary>
        /// Submits the details of the vehicle obtained in the GUI via the button "Submit Report" to the list Vehicles. Also
        /// checks for empty space and gives a prompt to add information if missing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void BSubmit_Click(object sender, RoutedEventArgs e)


        {
            if (string.IsNullOrWhiteSpace(txtID.Text))
            {
                MessageDialog dialog = new MessageDialog("Please enter an ID.");
                await dialog.ShowAsync();
                return;
            }
            if (string.IsNullOrWhiteSpace(txtMake.Text))
            {
                MessageDialog dialog = new MessageDialog("Please enter the make.");
                await dialog.ShowAsync();
                return;
            }
            if (string.IsNullOrWhiteSpace(txtPrice.Text))
            {
                MessageDialog dialog = new MessageDialog("Please enter the price.");
                await dialog.ShowAsync();
                return;
            }
            if (string.IsNullOrWhiteSpace(txtMile.Text))
            {
                MessageDialog dialog = new MessageDialog("Please enter the mileage.");
                await dialog.ShowAsync();
                return;
            }
            if (string.IsNullOrWhiteSpace(txtDep.Text))
            {
                MessageDialog dialog = new MessageDialog("Enter the insurance depreciation.");
                await dialog.ShowAsync();
                return;
            }

            Vehicle vehicle = new Vehicle(
                int.Parse(txtID.Text),
                txtMake.Text,
                decimal.Parse(txtPrice.Text),
                int.Parse(txtModel.SelectedIndex.ToString()),
                decimal.Parse(txtMile.Text),
                decimal.Parse(txtDep.Text));

            if (vehicles.AddVehicleToList(vehicle) == 1)
            {

                MessageDialog dialog = new MessageDialog("Already added");
                await dialog.ShowAsync();
            }

        }
        /// <summary>
        /// Uses the "Show Report" button on the GUI to show the report via message dialog windows. Shows two windows. The first for the list of all
        /// vehicles from lowest to highest depreciated value. Then a window for sum of all depreciated values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void BReport_Click(object sender, RoutedEventArgs e)

        {
            int carId = int.Parse(txtID.Text);
            Vehicle vehicle = vehicles.SearchVehicle(carId);
            if (vehicles.AddVehicleToList(vehicle) == 1)
            {

                MessageDialog dialog = new MessageDialog(vehicles.DisplayList(), "Report:");
                await dialog.ShowAsync();


                MessageDialog dialog1 = new MessageDialog(vehicles.DisplayList1().ToString(), "Total Lot Value ($):");
                await dialog1.ShowAsync();
            }
            if (vehicles.AddVehicleToList(vehicle) == 0)
            {
                MessageDialog dialog = new MessageDialog("No report");
                await dialog.ShowAsync();
            }



        }
    }
}

