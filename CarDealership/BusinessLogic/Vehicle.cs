﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.BusinessLogic
{   /// <summary>
/// Gets and sets all variables used in the XAML window for use with Vehicle list
/// </summary>
    class Vehicle
    {

        public int CarID { get; set; }
        public string CarMake { get; set; }
        public decimal CarPrice { get; set; }
        public int Model { get; set; }
        public decimal Mileage { get; set; }
        public decimal Deprec { get; set; }
        public decimal totalDepreciation { get; set; }



        public Vehicle(int carId, string make, decimal price, int model, decimal mileage, decimal depreciation)
        {
            CarID = carId;
            CarMake = make;
            CarPrice = price;
            Model = model;
            Mileage = mileage;
            Deprec = depreciation;
        }
        /// <summary>
        /// creates method for finding depreciated car value
        /// </summary>
        /// <returns></returns>
        public decimal FindDepreciation()
        {

            Depreciation depreciation = new Depreciation(CarPrice, Model, Deprec, Mileage);
            decimal totalDepreciation = depreciation.CarDeprec;
            return totalDepreciation;

        }




    }
}

