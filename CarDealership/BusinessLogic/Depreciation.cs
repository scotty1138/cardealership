﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.BusinessLogic
{/// <summary>
/// adds getters and setters for values used in Depreciation class.
/// </summary>
    class Depreciation

    {
        public decimal DepPrice { get; set; }
        public decimal CarPrice { get; set; }
        public int Model { get; set; }
        public decimal Deprec { get; set; }
        public decimal Mileage { get; set; }

        const decimal Year = 0.10M;
        const decimal Mile = 0.025M;
        const decimal accident = 0M;

        public Depreciation(decimal price, int model, decimal depreciation, decimal mileage)
        {
            CarPrice = price;
            Model = model;
            Deprec = depreciation;
            Mileage = mileage;

        }
        /// <summary>
        /// method CarDeprec used for calculating depreciated value of car.
        /// </summary>
        public decimal CarDeprec
        {
            get
            {
                decimal age = 1 - (Model * Year);
                if (age == 0)
                {
                    age = 1;
                }
                decimal distance = 1 - ((Mileage / 10000M) * Mile);
                if (Mileage <= 10000)
                {
                    distance = 1;
                }
                decimal accident = CarPrice * (Deprec / 100M);
                if (accident == 0)
                {
                    accident = 0;
                }
                decimal DepPrice = (((CarPrice * age) * distance) - accident);

                return DepPrice;
            }

        }
    }
}