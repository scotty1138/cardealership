﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CarDealership.BusinessLogic
{/// <summary>
/// Dealership class creates Vehicle list to store all information obtained from MainPage.xaml. 
/// </summary>
    class Dealership

    {

        List<Vehicle> vehicles;



        public Dealership()
        {
            vehicles = new List<Vehicle>();


        }


        public Vehicle SearchVehicle(int carId)
        {
            foreach (Vehicle vehicle in vehicles)
            {
                if (vehicle.CarID == carId)
                    return vehicle;
            }
            return null;
        }

        public int AddVehicleToList(Vehicle vehicle)
        {
            if (SearchVehicle(vehicle.CarID) == null)
            {
                vehicles.Add(vehicle);
                return 0;
            }
            return 1;

        }
        /// <summary>
        /// creates two methods for displaying information when "Show Report" button is clicked. The first orders the first list from lowest to highest depreciation.
        /// Second method creates total of all depreciated vehicles.
        /// </summary>
        /// <returns></returns>
        public string DisplayList()

        {
            string msg = "";
            foreach (Vehicle vehicle in vehicles)
            {
                msg += String.Format("ID: {0}  Make: {1}  Price: ${2}  Model: {3}  Mileage: {4}Km  Insurance Depreciation: {5}%  Depreciated Value: ${6}", vehicle.CarID, vehicle.CarMake, vehicle.CarPrice, vehicle.Model, vehicle.Mileage, vehicle.Deprec, vehicle.FindDepreciation());
                msg += Environment.NewLine;
                vehicles.Sort((a, b) => (a.FindDepreciation().CompareTo(b.FindDepreciation())));

            }
            return msg;

        }

        public decimal DisplayList1()

        {
            decimal totalcost = 0;
            foreach (Vehicle vehicle in vehicles)
            {
                decimal x = vehicle.FindDepreciation();
                totalcost += x;
            }
            return totalcost; ;
        }


    }



}


